from django.apps import AppConfig


class FillTheEmptyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fill_the_empty'
