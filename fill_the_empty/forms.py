from django import forms
from django.contrib.auth.models import User
from .models import Post
from datetime import datetime

class FillTheEmptyForm(forms.ModelForm):
    author = forms.CharField(label='Quem sou eu:', widget=forms.TextInput)
    text = forms.CharField(widget=forms.Textarea,
                           label='', required=True)
    class Meta:
        model = Post
        fields = ('author', 'text')
        exclude = ('control',)


class AdminTheEmptyForm(forms.ModelForm):
    #delete = forms.BooleanField(required=False)
    class Meta:
        model = Post
        fields = ('control', 'author', 'text')#, 'display_count')


