from django.db import models

# Create your models here.
CHOICES = (
    ('V', 'Valid'),
    ('I', 'Invalid'),
    ('U', 'Unknown'),
)

class Post(models.Model):
    author = models.CharField(max_length=255, default='anónimo')
    pub_date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    control = models.CharField(max_length=16, choices=CHOICES, default='U')
    #display_count = models.IntegerField(default=0)
    #image = models.ImageField(upload_to='images/')
 
    def summary(self):
        return self.text[:100]
    def pub_date_pretty(self):
        return self.pub_date.strftime('%b %e, %H:%M')
    def __str__(self):
        return self.author
