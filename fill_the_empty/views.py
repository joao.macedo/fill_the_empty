from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, JsonResponse
from .models import Post
from .forms import FillTheEmptyForm, AdminTheEmptyForm
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect

# Create your views here.
 
def _onebyone(request):
    pk_current = request.GET.get('pk_current')
    pk_latest = request.GET.get('pk_latest')
    valid_posts = Post.objects.filter(control='V')
    if pk_current is None:
        return valid_posts.first()
    recent_posts = valid_posts.filter(
        pk__gt=pk_current
    )
    if any(recent_posts):
        return recent_posts.first()
    else:
        return valid_posts.first()


def json_onebyone(request):
    post = _onebyone(request)
    data = {'author': post.author, 'text': post.text,
            'pub_date': post.pub_date_pretty(), 'pk': post.pk}
    return JsonResponse(data, content_type="application/json")


def fill_the_empty(request):
    if request.method == 'POST':
        insert_form = FillTheEmptyForm(request.POST)
        if insert_form.is_valid():
            properties = insert_form.save(commit=False)
            properties.user = "anonymous"
            properties.save()
            return HttpResponseRedirect('fill_the_empty')
    else:
        insert_form = FillTheEmptyForm()
    args = {}
    args['request'] = request
    args['form'] = insert_form
    return render(request, 'post/insert_text.html', args)


@login_required
@csrf_protect
def admin_the_empty(request):
    posts = Post.objects
    if request.method == "POST":
        admin_forms = [AdminTheEmptyForm(request.POST, prefix=str(post.id),
                          instance=post) for post in posts.all()]
        if all([v_form.is_valid() for v_form in admin_forms]):
            for admin_form in admin_forms:
                properties = admin_form.save(commit=False)
                print(properties.__dict__)
                properties.user = request.user
                properties.save()
            return HttpResponseRedirect('admin_the_empty')
    else:
        admin_forms = [AdminTheEmptyForm(prefix=str(post.id),
                          instance=post) for post in posts.all()]
    args = {}
    args['request'] = request
    args['forms'] = admin_forms
    return render(request, 'post/admin_text.html', args)




