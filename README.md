Create database migrations
```
python manage.py makemigrations
python manage.py migrate --run-syncdb
```

Create superuser
```
python manage.py createsuperuser
```

Run server
```
python manage.py runserver
```
or using gunicorn
'''
gunicorn fiar2021.wsgi
'''

Login Django admin website with your superadmin account credentials at http://127.0.0.1:8000/admin

Create user: *anonymous* and give User permissions to create Post. 
(Note this username is hardcoded in the *fill_the_empty* view, at views.py ), 
```
User permissions:
fill_the_empty | post | Can add post

SAVE
```



Add bootstrap:

npm install bootstrap bootstrap.css jquery

heroku buildpacks:add --index 1 heroku/nodejs
 ›   Warning: heroku update available from 7.52.0 to 7.53.0.
Buildpack added. Next release on fiar2021 will use:
  1. heroku/nodejs
  2. heroku/python
Run git push heroku main to create a new release using these buildpacks.
